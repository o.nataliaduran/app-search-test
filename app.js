const express = require('express');
const app = express();
const port = 3000;
const fetch = require("node-fetch");

// Motor de plantilla por las dudas
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

// Middleware para archivos estáticos 
app.use(express.static(__dirname + '/public'));

// Las rutas
app.use('/', require('./router/routes'));


// ---------------------------------------------------------------------
// const url = "https://jsonplaceholder.typicode.com/posts/1";
// const url2 = "http://d01.biblos.intranet.osde:3002/api/as/v1/engines/biblos/search.json";

// const getData = async url => {
//   try {
//     const response = await fetch(url, {
//       method: 'GET',
//       body: { 'query': 'terapéutica', 'page': {'size': 7} },
//       headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer private-encd1mfhj1ee74d1jd7jhq9s' },
//   });
//     const json = await response.json();
//     console.log(json);
//   } catch (error) {
//     console.log(error);
//   }
// };

// getData(url2);
// ---------------------------------------------------------------------
// const cors = require('cors');

// app.use(cors(true));
// app.options('', cors());
// app.use(methodOverride());
// app.use(bodyParser.json()) // support JSON-encoded bodies
// app.use(bodyParser.urlencoded({ extended: true })) // support URL-encoded bodies

// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "");
//     res.header('Access-Control-Expose-Headers', 'Access-Control-*, Origin, X-Requested-With, Content-Type, Accept, Authorization');
//     res.header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     res.header('Access-Control-Allow-Credentials', true);
//     res.header("Access-Control-Max-Age", "1728000");
//     next();
// });

const axios = require("axios");
const token = "private-encd1mfhj1ee74d1jd7jhq9s";
axios.defaults.headers.common = {'Authorization': `Bearer ${token}`};

axios.get( 
'http://d01.biblos.intranet.osde:3002/api/as/v1/engines/biblos/search.json',
{
  params: {
    query: 'terapéutica'
  }
})
.then((response) => {
  console.log(response.data);
}).catch(console.log);


// const getData = async url => {
//   try {
//     const response = await axios.get(url, { headers: {'Authorization': 'Bearer private-encd1mfhj1ee74d1jd7jhq9sh' }, body: { 'query': 'terapéutica', 'page': {'size': 7} }});
//     const data = response.data;
//     console.log(data);
//   } catch (error) {
//     console.log(error);
//   }
// };

// getData(url3);


// ---------------------------------------------------------------------

// let todo = {
//     query: "terapéutica",
//     page: {"size": 7}
// };

// fetch('http://d01.biblos.intranet.osde:3002/api/as/v1/engines/biblos/search.json', {
//     method: 'GET',
//     body: JSON.stringify(todo),
//     headers: { 'Authorization': 'Bearer private-encd1mfhj1ee74d1jd7jhq9s' , 'Content-Type': 'application/json' }
// }).then(res => res.json())
//   .then(json => console.log(json))
//   .catch(function (error) {
//         // handle error
//         console.log('EL ERROOOR', error);
//       });

app.listen(port, () => {
  console.log(`Servidor escuchando en el puerto ${port}`);
});